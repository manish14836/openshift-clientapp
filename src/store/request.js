/* global fetch */
import 'whatwg-fetch';

const uuidv4 = require('uuid/v4');
export const baseUri = (process.env.API_BASE_URL || "http://localhost:8003");


const makeHeaders = (extraHeaders) => {

 if (localStorage.getItem('session_id')) {
    return {
      headers: {
        Accept: 'application/json',
        Authorization: localStorage.getItem('session_id'),
        'Access-Control-Allow-Origin': '*',
        'x-fapi-interaction-id':uuidv4(),
      },
    };
  }
  return {
    headers: {
      Accept: 'application/json',
     'Access-Control-Allow-Origin': '*',
      'x-fapi-interaction-id':uuidv4(),
    },
  };
};

const addMoreHeaders = (headers,extraHeaders) => {

    return Object.assign(headers,extraHeaders);
  }
const makeErrorResponse = async (response) => {
  return { 'error' : {'errorstatus' : response.status,'errorText' : response.statusText}
};

}

const isValidJson = function (jsonData) {
        let isValidFlag = true;
        try {
                JSON.parse(jsonData);
            } catch (ex) {
              isValidFlag = new Boolean(false);
            }
            return isValidFlag;
  }

const asyncAwaitPostJson = async (endpoint, authServerId, data, ipd, unauthorizedType,extraHeaders,interactionId) => {
  console.log('Inside asyncAwaitPostJson');
   const uri = `${baseUri}${endpoint}`;
   console.log(uri);
  const { headers } = makeHeaders(authServerId);
  headers['Content-Type'] = 'application/json';
  if(ipd !==undefined) {
      headers['x-fapi-customer-ip-address'] = ipd;
  }
  if((extraHeaders !== null) && typeof extraHeaders !=='undefined' && (extraHeaders.constructor === Object)) {
     Object.assign(headers,extraHeaders);
  }
  if(interactionId !==undefined) {
      headers['x-fapi-interaction-id'] = interactionId;
  }

  const response = await fetch(uri, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  });
  if (response.status === 200) {
    return response.json();
  } else if(response.status === 204)
  {
     return true;
  } else {
  return makeErrorResponse(response);
  }
  return null;
};

const asyncAwaitPost = async (endpoint, data, unauthorizedType) => {
  
  const response = await fetch(`${baseUri}${endpoint}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: 'application/json',
    },
    // for now set body string to emulate x-www-form-urlencoded
    body: data,
  });
  if (response.status === 200) {
    const json = await response.json();
    return json;
  } else if (response.status === 401) {
    return unauthorizedType;
  } else if(response.status === 204) {
     return true;
  }
  else {
    return  makeErrorResponse(response);
  }
  return null;
};

const asyncAwaitGetRequest = async (endpoint, unauthorizedType, authServerId,extraHeaders) => {
  let uri;
  let sendData;
  if (authServerId) {
    uri = `${baseUri}${endpoint}`;
    sendData = makeHeaders(authServerId);
  } else {
    uri = `${baseUri}${endpoint}`;
    sendData = makeHeaders();
  }
  if((extraHeaders !== null) && typeof extraHeaders !=='undefined' && (extraHeaders.constructor === Object)) {
      sendData.headers = Object.assign(sendData.headers,extraHeaders);
  }
  const response = await fetch(uri, sendData);

  switch (parseInt(response.status, 0)) {
    case 200: {
      const json = await response.json();
      return json;
    }
    case 204: {
      return response;
    }
    default: {
      return makeErrorResponse(response);
    }
  }
};

const asyncAwaitRootGetRequest = async (endpoint, unauthorizedType, authServerId,extraHeaders) => {
  let uri;
  let sendData;
    uri = `${baseUri}${endpoint}`;
  if (authServerId) {
    sendData = makeHeaders(authServerId);
  } else {
    sendData = makeHeaders();
  }
  if((extraHeaders !== null) && typeof extraHeaders !=='undefined' && (extraHeaders.constructor === Object)) {
      sendData.headers = Object.assign(sendData.headers,extraHeaders);
  }
  const response = await fetch(uri, sendData);

  switch (parseInt(response.status, 0)) {
    case 200: {
      const json = await response.json();
      return json;
    }
    case 204: {
      return response;
    }
    default: {
      return makeErrorResponse(response);
    }
  }
};

export const postJson = asyncAwaitPostJson;
export const request = asyncAwaitGetRequest;
export const post = asyncAwaitPost;
export const rootGetRequest = asyncAwaitRootGetRequest;

import Vue from 'vue';
import { postJson, request,rootGetRequest } from '../request';

import {
  CUSTOMER_DETAILS,
  ERROR,
  ACCOUNT_DETAILS,
  TRANSACTIONS_DETAILS
} from '../mutation-types';

const initialState = {
  aspsps: [],
  customerDetails: null,
  accounts: [],
  transactions :[],
  error : {}
};
const saved = Object.assign({},initialState);

const getters = {
  customerDetails: state => state.customerDetails,
  accounts:state => state.accounts,
  transactions:state =>state.transactions,
  error:state =>state.error
};

const mutations = {

  [CUSTOMER_DETAILS](state, customer) {
    let customerData = JSON.parse(customer);
    console.log(customerData);
    Vue.set(state, 'customerDetails',customerData.customerDetails);
  },
  [ACCOUNT_DETAILS](state, accountsDetails) {
    Vue.set(state, 'accounts',accountsDetails);
  },
  [TRANSACTIONS_DETAILS](state, transactionsDetails) {
    Vue.set(state, 'transactions',transactionsDetails);
  },
  [ERROR](state, payload) {
    state.error = payload.error;
  },
};

export default {
  state: initialState,
  getters,
  mutations
};

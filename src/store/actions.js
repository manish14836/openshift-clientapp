import { request, post, postJson, tppAuthCodeUri,rootGetRequest } from './request';
import * as types from './mutation-types';

const actions = {
  async createSession({ commit }, credentials) {
    commit(types.LOGIN,credentials);
    // set body string to emulate x-www-form-urlencoded form
    const body = `u=${credentials.u}&p=${credentials.p}`;
    const result = await post('/login', body, types.LOGIN_INVALID_CREDENTIALS);
    if (result === types.LOGIN_INVALID_CREDENTIALS) {
      return commit(types.LOGIN_INVALID_CREDENTIALS);
    } 
    else if (result.error) {
       return commit(types.LOGIN_ERROR);
    }
    else if (result) {
      localStorage.setItem('session_id', result.sid);
    //  console.log(result.customer);
      commit(types.CUSTOMER_DETAILS,result.customer);
      return commit(types.LOGIN_SUCCESS);
    }
    return commit(types.LOGIN_ERROR);
  },
  async deleteSession({ dispatch,commit }) {
    await request('/logout');
    localStorage.removeItem('session_id');
    return commit(types.LOGOUT);
  },
  async accounts({ commit, dispatch, getters }) {

    let customerDetails = this.getters.customerDetails;
    console.log(customerDetails.partySysId+"sd");
    const response = await request(`/api/account/v1/${customerDetails.partySysId}`, types.LOGOUT);
    if (response === types.LOGOUT) {
      return dispatch('deleteSession');
    } else if (response) {
      if(response.error) {
      return commit(types.ERROR, {
        error : response.error,
      });
    }
    //  console.log(response);
      return commit(types.ACCOUNT_DETAILS, response.response);
    }
    return dispatch('deleteSession');
  },
  async transactions({ commit, dispatch, getters },payload) {

    let account_id = payload.account_id;
    
    const response = await request(`/api/account/v1/${account_id}/transaction`, types.LOGOUT);
    if (response === types.LOGOUT) {
      return dispatch('deleteSession');
    } else if (response) {
      if(response.error) {
      return commit(typesERROR, {
        error : response.error,
      });
    }
    //  console.log(response);
      return commit(types.TRANSACTIONS_DETAILS, response.response);
    }
    return dispatch('deleteSession');
  },

};

export default actions;

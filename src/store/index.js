import Vue from 'vue';
import Vuex from 'vuex';
import customer from './modules/customer';
import session from './modules/session';
import actions from './actions';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    customer,
    session
  },
  actions,
});

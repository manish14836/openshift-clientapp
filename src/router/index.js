import Vue from 'vue';
import Router from 'vue-router';

import AccountsList from '@/components/AccountsList';
import Login from '@/components/Login';
import TransactionList from '@/components/TransactionList';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/accounts',
      name: 'AccountsList',
      component: AccountsList,
    },
    {
      path: '/transactions',
      name: 'TransactionList',
      component: TransactionList,
    },
    {
      path: '/',
      name: 'Login',
      component: Login,
    },

  ],
});

router.beforeEach((to, from, next) => {
  const loggedIn = router.app.$store.getters.isLoggedIn;
  if (!loggedIn && to.path !== '/') {
    next({
      path: '/',
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
});

export default router;
